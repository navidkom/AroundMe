package ir.artapps.aroundme.view.adapter.viewholder;

import android.view.View;

import ir.artapps.aroundme.entities.Venue;
import ir.artapps.aroundme.view.adapter.MainRecyclerViewAdapter;

public class LoadingViewHolder extends BaseViewHolder{

    public LoadingViewHolder(View v) {
        super(v);
    }

    @Override
    public void onBind(Venue venue) {

    }

    @Override
    public void setClickListener(MainRecyclerViewAdapter.OnItemClickListener listener) {

    }
}
