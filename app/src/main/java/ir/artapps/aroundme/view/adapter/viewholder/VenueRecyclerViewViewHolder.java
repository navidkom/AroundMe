package ir.artapps.aroundme.view.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ir.artapps.aroundme.R;
import ir.artapps.aroundme.entities.Venue;
import ir.artapps.aroundme.util.DistanceUtil;
import ir.artapps.aroundme.util.ImageUtil;
import ir.artapps.aroundme.view.adapter.MainRecyclerViewAdapter;

public class VenueRecyclerViewViewHolder extends BaseViewHolder implements View.OnClickListener {
    public TextView name;
    public TextView  distance;
    public ImageView imageView;
    MainRecyclerViewAdapter.OnItemClickListener listener;

    public VenueRecyclerViewViewHolder(View v) {
        super(v);
        v.setOnClickListener(this);
        name = v.findViewById(R.id.venue_recycler_item_name_text_view);
        distance = v.findViewById(R.id.venue_recycler_item_distance_text_view);
        imageView = v.findViewById(R.id.venue_recycler_item_category_image_view);

    }

    @Override
    public void onBind(Venue model) {
        name.setText(model.getName());
        distance.setText(DistanceUtil.distanceToString(model.getDistance()));
        imageView.setImageBitmap(null);
        ImageUtil.setImage(model.getIcon(), imageView);
    }


    @Override
    public void setClickListener(MainRecyclerViewAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null) {
            listener.onItemClick(v, getAdapterPosition());
        }
    }
}
