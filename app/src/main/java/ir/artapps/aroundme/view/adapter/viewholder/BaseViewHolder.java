package ir.artapps.aroundme.view.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ir.artapps.aroundme.entities.Venue;
import ir.artapps.aroundme.view.adapter.MainRecyclerViewAdapter;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder{
    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void onBind(Venue venue);
    public abstract void setClickListener(MainRecyclerViewAdapter.OnItemClickListener listener);
}
