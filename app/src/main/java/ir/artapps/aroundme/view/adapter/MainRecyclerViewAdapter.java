package ir.artapps.aroundme.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ir.artapps.aroundme.R;
import ir.artapps.aroundme.entities.Venue;
import ir.artapps.aroundme.view.adapter.viewholder.BaseViewHolder;
import ir.artapps.aroundme.view.adapter.viewholder.LoadingViewHolder;
import ir.artapps.aroundme.view.adapter.viewholder.VenueRecyclerViewViewHolder;

/**
 * Created by navid on 28,December,2018
 */
public class MainRecyclerViewAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<Venue> items;
    private boolean addLoading = false;
    private OnItemClickListener mItemClickListener;
    private final int VIEW_TYPE_VENUE = 0;
    private final int VIEW_TYPE_LOADING = 1;


    public MainRecyclerViewAdapter(List<Venue> items, final OnItemClickListener mItemClickListener) {
        this.items = items;
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_VENUE:
                return new VenueRecyclerViewViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.venues_recycler_view_view_holder, parent, false));
            default:
                return new LoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.loading_view_holder, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        Venue venue = null;
        if (getItemViewType(i) == VIEW_TYPE_VENUE) {
            venue = items.get(i);
        }
        baseViewHolder.onBind(venue);
        baseViewHolder.setClickListener(mItemClickListener);
    }

    @Override
    public int getItemCount() {
        if (addLoading) {
            return items.size() + 1;
        }
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        if ( position == items.size() ) {
            return VIEW_TYPE_LOADING;
        }
        return VIEW_TYPE_VENUE;
    }

    public void setLoading(boolean loading) {
        addLoading = loading;
        notifyItemChanged(items.size());
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}